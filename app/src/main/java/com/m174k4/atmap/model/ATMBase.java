package com.m174k4.atmap.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.m174k4.atmap.R;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class ATMBase extends RealmObject implements ClusterItem {
	@Ignore
	private final LatLng mPosition;

	private String bank;
	private double lat;
	private double lon;

	public ATMBase() {
		mPosition = new LatLng(0, 0);
	}

	public ATMBase(ATMBase atm) {
		this.mPosition = new LatLng(atm.getLat(), atm.getLon());
		this.bank = atm.getBank();
		this.lat = atm.getLat();
		this.lon = atm.getLon();
	}

	public void saveBankEnum(Banks val) {
		this.bank = val.toString();
	}

	public Banks getBankEnum() {
		return Banks.valueOf(bank);
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	@Override
	public LatLng getPosition() {
		return mPosition;
	}

	public int getBankName() {
		switch (getBankEnum()) {
			case DSK:
				return (R.string.Bank_Name_DSK);
			case UBB:
				return (R.string.Bank_Name_UBB);
			case FIB:
				return (R.string.Bank_Name_FIB);
			case RAIFAIZEN:
				return (R.string.Bank_Name_Raifaizen);
			case UNICREDIT:
				return (R.string.Bank_Name_Unicredit);
			case SGEB:
				return (R.string.Bank_Name_SGEB);
			case ALLIANZ:
				return (R.string.Bank_Name_Allianz);
			case PIRAEUS:
				return (R.string.Bank_Name_Piraeus);
			case IBANK:
				return (R.string.Bank_Name_IBank);
			case IAB:
				return (R.string.Bank_Name_IAB);
			case MUNICIPAL_BANK:
				return (R.string.Bank_Name_MunicipalBank);
			case POST_BANK:
				return (R.string.Bank_Name_PostBank);
			case PROCREDIT:
				return (R.string.Bank_Name_Procredit);
			case CIBANK:
				return (R.string.Bank_Name_CiBank);
			case TOKUDA:
				return (R.string.Bank_Name_Tokuda);
			case DBANK:
				return (R.string.Bank_Name_DBank);
			case CCB:
				return (R.string.Bank_Name_CCB);
			default:
				return (R.string.Bank_Name_DSK);
		}
	}

	public int getBankIcon() {
		switch (getBankEnum()) {
			case DSK:
				return (R.drawable.ic_marker_dsk);
			case UBB:
				return (R.drawable.ic_marker_ubb);
			case FIB:
				return (R.drawable.ic_marker_fib);
			case RAIFAIZEN:
				return (R.drawable.ic_marker_raifaizen);
			case UNICREDIT:
				return (R.drawable.ic_marker_unicredit);
			case SGEB:
				return (R.drawable.ic_marker_sgeb);
			case ALLIANZ:
				return (R.drawable.ic_marker_allianz);
			case PIRAEUS:
				return (R.drawable.ic_marker_piraeus);
			case IBANK:
				return (R.drawable.ic_marker_ibank);
			case IAB:
				return (R.drawable.ic_marker_iab);
			case MUNICIPAL_BANK:
				return (R.drawable.ic_marker_municipal);
			case POST_BANK:
				return (R.drawable.ic_marker_postbank);
			case PROCREDIT:
				return (R.drawable.ic_marker_procredit);
			case CIBANK:
				return (R.drawable.ic_marker_cibank);
			case TOKUDA:
				return (R.drawable.ic_marker_tokuda);
			case DBANK:
				return (R.drawable.ic_marker_dbank);
			case CCB:
				return (R.drawable.ic_marker_ccb);
			default:
				return (R.drawable.ic_marker_dsk);
		}
	}
}

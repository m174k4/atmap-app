package com.m174k4.atmap.model;

import java.util.List;

public interface ATMProvider {
	List<ATMBase> getATMs();
}

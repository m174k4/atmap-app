package com.m174k4.atmap.model;

import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;

import com.m174k4.atmap.utils.BanksPreferences;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class ATMProviderImpl implements ATMProvider {
	private final Context mContext;
	private final Realm mRealm;
	private final List<ATMBase> mATMList = new ArrayList<>();

	private final Menu mMenu;

	public ATMProviderImpl(Context context, Realm realm, Menu menu) {
		mContext = context;
		mRealm = realm;
		mMenu = menu;
	}

	public void fetch() {
		populateATMList();
	}

	private void populateATMList() {
		mATMList.clear();
		BanksPreferences prefs = new BanksPreferences();
		boolean isFirst = true;
		RealmQuery<ATMBase> realmQuery = mRealm.where(ATMBase.class);
		if (prefs.getAllState(mContext)) {
			isFirst = false;
		} else {
			int menuItemsCount = mMenu.size();
			for (int i = 1; i < menuItemsCount; i++) {
				MenuItem menuItem = mMenu.getItem(i);
				int menuItemId = menuItem.getItemId();
				if (prefs.getState(mContext, menuItemId)) {
					if (isFirst) {
						realmQuery.equalTo("bank", prefs.getBank(menuItemId).name());
						isFirst = false;
					} else {
						realmQuery.or().equalTo("bank", prefs.getBank(menuItemId).name());
					}
				}
			}
		}
		if (!isFirst) { //This checks if the settings are all off
			RealmResults<ATMBase> atms = realmQuery.findAll();
			for (ATMBase atm : atms) {
				mATMList.add(new ATMBase(atm));
			}
		}
	}

	@Override
	public List<ATMBase> getATMs() {
		return mATMList;
	}
}

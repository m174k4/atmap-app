package com.m174k4.atmap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.m174k4.atmap.model.ATMProviderImpl;
import com.m174k4.atmap.utils.ATMRenderer;
import com.m174k4.atmap.utils.CustomClusterManager;
import com.m174k4.atmap.utils.LocationProvider;

public class MainActivity extends NavDrawerActivity implements OnMapReadyCallback, LocationProvider.LocationCallback {

	private ATMProviderImpl mATMProvider;
	private GoogleMap mMap;
	private CustomClusterManager mClusterManager;
	private LocationProvider mLocationProvider;
	private boolean mIsCameraCentered = false;
	private boolean mUseLocation = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

		setStuff();
		mATMProvider = new ATMProviderImpl(getApplicationContext(), mRealm, binding.navigationView.getMenu());

		mLocationProvider = new LocationProvider(this, this);

		setUpMap();
		checkForLocAccess();
	}

	private void checkForLocAccess() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			mUseLocation = false;
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LocationProvider.PERMISSION_LOCATION_CODE);
		} else {
			mUseLocation = true;
			mLocationProvider.requestLocationAccess();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mUseLocation)
			mLocationProvider.connect();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mLocationProvider.disconnect();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mLocationProvider.destroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == LocationProvider.REQUEST_CHECK_SETTINGS) {
			if (resultCode == RESULT_OK) {
				mLocationProvider.connect();
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {
			case LocationProvider.PERMISSION_LOCATION_CODE:
				if (grantResults.length > 0) {
					if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
						mUseLocation = false;
						mLocationProvider.disconnect(); // TODO: research if there is battery drain or other problems
					} else {
						mUseLocation = true;
						setMyLocationEnabled();
						mLocationProvider.requestLocationAccess();
					}
				}
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
		mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
			@Override
			public boolean onMyLocationButtonClick() {
				mLocationProvider.requestLocationAccess();
				return false;
			}
		});
		setUpClusterer();
		setMyLocationEnabled();
	}

	private void setMyLocationEnabled() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}
		if (mUseLocation && mMap != null)
			mMap.setMyLocationEnabled(true);
	}

	public void handleLocationUpdate(Location location) {
		centerCamera(location);
	}

	@Override
	public void handleBanksChange() {
		refreshClusterer();
	}

	private void setUpMap() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
		mapFragment.getMapAsync(this);
	}

	private void setUpClusterer() {
		mClusterManager = new CustomClusterManager(this, mMap, mATMProvider);
		mClusterManager.setRenderer(new ATMRenderer(getApplicationContext(), mMap, mClusterManager));

		mMap.setOnCameraIdleListener(mClusterManager);
		mMap.setOnMarkerClickListener(mClusterManager);
		refreshClusterer();
	}

	private void refreshClusterer() {
		mATMProvider.fetch();
		mClusterManager.onCameraIdle();
	}

	private void centerCamera(Location location) {
		if (location != null && !mIsCameraCentered) {
			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(location.getLatitude(), location.getLongitude()))
					.zoom(17)
					.build();
			mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			mIsCameraCentered = true;
		}
	}
}
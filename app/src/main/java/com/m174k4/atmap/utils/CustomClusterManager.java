package com.m174k4.atmap.utils;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;
import com.m174k4.atmap.model.ATMProvider;
import com.m174k4.atmap.model.ATMBase;

public class CustomClusterManager extends ClusterManager<ATMBase> {
	private final GoogleMap mMap;
	private final ATMProvider mATMProvider;
	private CameraPosition mPreviousCameraPosition;

	public CustomClusterManager(Context context, GoogleMap map, ATMProvider ATMProvider) {
		super(context, map);

		mMap = map;
		mATMProvider = ATMProvider;
	}

	@Override
	public void onCameraIdle() {
		LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

		//Expands the bounds by ~300m to improve panning experience.
		LatLng latLng1 = new LatLng(bounds.southwest.latitude - 0.003, bounds.southwest.longitude - 0.003);
		LatLng latLng2 = new LatLng(bounds.northeast.latitude + 0.003, bounds.northeast.longitude + 0.003);
		LatLngBounds correctedBounds = new LatLngBounds(latLng1, latLng2);

		clearItems();

		for (ATMBase atm : mATMProvider.getATMs()) {
			LatLng markerPoint = new LatLng(atm.getLat(), atm.getLon());
			if (correctedBounds.contains(markerPoint)) {
				addItem(atm);
			}
		}

		CameraPosition position = mMap.getCameraPosition();
		if (mPreviousCameraPosition == null || mPreviousCameraPosition.zoom != position.zoom) {
			mPreviousCameraPosition = position;
		}

		if (mPreviousCameraPosition.zoom == position.zoom) {
			mPreviousCameraPosition = position;
			cluster();
		}

		super.onCameraIdle();
	}
}

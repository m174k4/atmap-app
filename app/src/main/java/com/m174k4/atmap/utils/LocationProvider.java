package com.m174k4.atmap.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.m174k4.atmap.R;

public class LocationProvider implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

	public interface LocationCallback {
		void handleLocationUpdate(Location location);
	}

	private static final String TAG = LocationProvider.class.getSimpleName();

	private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	public static final int PERMISSION_LOCATION_CODE = 100;
	public static final int REQUEST_CHECK_SETTINGS = 444;

	private final GoogleApiClient mGoogleApiClient;
	private final LocationRequest mLocationRequest;
	private Context mContext;
	private LocationCallback mLocationCallback;

	public LocationProvider(Context context, LocationCallback callback) {
		mContext = context;

		mGoogleApiClient = new GoogleApiClient.Builder(context.getApplicationContext())
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();

		mLocationRequest = LocationRequest.create()
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
				.setInterval(15000)
				.setFastestInterval(7000);

		mLocationCallback = callback;
	}

	public void connect() {
		if (!mGoogleApiClient.isConnected())
			mGoogleApiClient.connect();
	}

	public void disconnect() {
		if (mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
			mGoogleApiClient.disconnect();
		}
	}

	public void destroy() {
		mLocationCallback = null;
		mContext = null;
	}

	public void requestLocationAccess() {
		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
		result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
			@Override
			public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
				final Status status = locationSettingsResult.getStatus();
				switch (status.getStatusCode()) {
					case LocationSettingsStatusCodes.SUCCESS:
						connect();
						break;
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						try {
							status.startResolutionForResult((Activity) mContext, REQUEST_CHECK_SETTINGS);
						} catch (IntentSender.SendIntentException e) {
							// Ignore the error.
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						disconnect();
						break;
				}
			}
		});
	}

	@Override
	public void onConnected(Bundle bundle) {
		if (ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}

		Log.i(TAG, "Location services connected.");

		Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (location == null) {
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
		} else {
			mLocationCallback.handleLocationUpdate(location);
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this); //TODO: This works, but research if better approach is available.
		}
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		if (connectionResult.hasResolution() && mContext instanceof Activity) {
			try {
				connectionResult.startResolutionForResult((Activity) mContext, CONNECTION_FAILURE_RESOLUTION_REQUEST);
			} catch (IntentSender.SendIntentException e) {
				// Thrown if Google Play services canceled the original PendingIntent
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.Error_Message_On_Connection_Failed, Toast.LENGTH_SHORT).show();
			Log.e(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		if (mLocationCallback != null)
			mLocationCallback.handleLocationUpdate(location);
	}
}
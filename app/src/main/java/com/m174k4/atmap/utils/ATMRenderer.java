package com.m174k4.atmap.utils;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.m174k4.atmap.model.ATMBase;

public class ATMRenderer extends DefaultClusterRenderer<ATMBase> {
	private final Context mContext;

	public ATMRenderer(Context context, GoogleMap map, ClusterManager<ATMBase> clusterManager) {
		super(context, map, clusterManager);

		mContext = context;
	}

	@Override
	protected void onBeforeClusterItemRendered(ATMBase atm, MarkerOptions markerOptions) {
		markerOptions.icon(BitmapDescriptorFactory.fromResource(atm.getBankIcon()));
		markerOptions.title(mContext.getString(atm.getBankName()));
	}

	@Override
	protected boolean shouldRenderAsCluster(Cluster cluster) {
		return cluster.getSize() > 5;
	}
}

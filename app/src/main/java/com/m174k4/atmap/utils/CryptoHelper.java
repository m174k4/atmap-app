package com.m174k4.atmap.utils;

/*
 * Created by Dimitar Ralev on 21.10.2016.
 */

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import io.realm.internal.IOException;

class CryptoHelper {
	private static final String CIPHER = "AES/CBC/PKCS5Padding";
	private static final String ALGORITHM = "PBKDF2WithHmacSHA1";

	byte[] readBytes(InputStream inputStream) throws IOException, java.io.IOException {
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		int len;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}

		return byteBuffer.toByteArray();
	}

	private byte[] getSalt() {
		NavigationViewListener l = new NavigationViewListener(null);
		BanksPreferences p = new BanksPreferences();
		byte[] salt = combine(l.getSalt1(), p.getSalt2());

		return fixSalt(salt);
	}

	private byte[] fixSalt(byte[] b) {
		b[1] = (byte) (b[1] - 2);
		b[5] = (byte) (b[5] + 1);
		b[8] = (byte) (b[8] - 6);
		b[11] = (byte) (b[11] - 8);
		b[13] = (byte) (b[13] + 4);
		return b;
	}

	private char[] getPass() {
		BanksPreferences p = new BanksPreferences();
		NavigationViewListener l = new NavigationViewListener(null);
		return (l.getP1() + p.getP2()).toCharArray();
	}

	private byte[] getIV() {
		NavigationViewListener l = new NavigationViewListener(null);
		BanksPreferences p = new BanksPreferences();
		return combine(p.getIv1(), l.getIv2());
	}

	private byte[] combine(byte[] a, byte[] b) {
		byte[] c = new byte[a.length + b.length];
		for (int i = 0; i < c.length; ++i) {
			c[i] = i < a.length ? a[i] : b[i - a.length];
		}
		return c;
	}

	private SecretKey generateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
		KeySpec spec = new PBEKeySpec(getPass(), getSalt(), 65536, 128);
		SecretKey tmp = factory.generateSecret(spec);
		return new SecretKeySpec(tmp.getEncoded(), "AES");
	}

	private IvParameterSpec generateIv() {
		return new IvParameterSpec(getIV());
	}

	byte[] decrypt(byte[] cipherText) throws Exception {
		Cipher cipher = Cipher.getInstance(CIPHER);
		cipher.init(Cipher.DECRYPT_MODE, generateKey(), generateIv());
		return cipher.doFinal(cipherText);
	}

	public byte[] getRealmKey() {
		BanksPreferences p = new BanksPreferences();
		NavigationViewListener l = new NavigationViewListener(null);
		byte[] f16 = combine(p.getSalt2(), l.getIv2());
		byte[] s16 = combine(p.getIv1(), l.getIv2());
		byte[] f32 = combine(f16, s16);
		byte[] s32 = combine(s16, f16);
		return combine(f32, s32);
	}
}

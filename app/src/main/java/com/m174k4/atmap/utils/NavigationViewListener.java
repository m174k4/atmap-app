package com.m174k4.atmap.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.view.MenuItem;
import android.widget.CompoundButton;

public class NavigationViewListener implements NavigationView.OnNavigationItemSelectedListener {

	private Context mContext;
	public NavigationViewListener(Context context) {
		mContext = context;
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
		CompoundButton switchView = (CompoundButton) MenuItemCompat.getActionView(item);

		if (switchView != null) {
			if (switchView.isChecked())
				switchView.setChecked(false);
			else
				switchView.setChecked(true);
		} else {
			if(mContext != null) {
				Uri uri = Uri.parse("http://infallible-elion-7b2bc3.netlify.com/");
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				mContext.startActivity(intent);
			}
		}

		return false;
	}

	byte[] getSalt1() {
		return salt1;
	}

	private final byte[] iv2 = new byte[]{(byte) 0xbe, (byte) 0x5e, (byte) 0x83, (byte) 0xce, (byte) 0xd7, (byte) 0xd1, (byte) 0xfe, (byte) 0x2f};

	private final byte[] salt1 = new byte[]{(byte) 0x3d, (byte) 0xba, (byte) 0xda, (byte) 0xb7, (byte) 0x8e, (byte) 0x79, (byte) 0x8d, (byte) 0xee};

	byte[] getIv2() {
		return iv2;
	}

	String getP1() {
		return "mrxsGs1n";
	}
}

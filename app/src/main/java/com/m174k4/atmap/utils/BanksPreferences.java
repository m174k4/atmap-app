package com.m174k4.atmap.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.m174k4.atmap.R;
import com.m174k4.atmap.model.Banks;

public class BanksPreferences {

	private static final String ALL_STATE = "com.m174k4.atmap.BANKS.All";

	private static final String DSK_STATE = "com.m174k4.atmap.BANKS.dsk";
	private static final String UBB_STATE = "com.m174k4.atmap.BANKS.ubb";
	private static final String FIB_STATE = "com.m174k4.atmap.BANKS.fib";
	private static final String RAIFAIZEN_STATE = "com.m174k4.atmap.BANKS.raifaizen";
	private static final String UNICREDIT_STATE = "com.m174k4.atmap.BANKS.unicredit";
	private static final String SGEB_STATE = "com.m174k4.atmap.BANKS.sgeb";
	private static final String ALIANZ_STATE = "com.m174k4.atmap.BANKS.alianz";
	private static final String PIRAEUS_STATE = "com.m174k4.atmap.BANKS.piraeus";
	private static final String IBANK_STATE = "com.m174k4.atmap.BANKS.ibank";
	private static final String IAB_STATE = "com.m174k4.atmap.BANKS.iab";
	private static final String MUNICIPAL_STATE = "com.m174k4.atmap.BANKS.municipal";
	private static final String POSTBANK_STATE = "com.m174k4.atmap.BANKS.postbank";
	private static final String PROCREDIT_STATE = "com.m174k4.atmap.BANKS.procredit";
	private static final String CIBANK_STATE = "com.m174k4.atmap.BANKS.cibank";
	private static final String TOKUDA_STATE = "com.m174k4.atmap.BANKS.tokuda";
	private static final String DBANK_STATE = "com.m174k4.atmap.BANKS.dbank";
	private static final String CCB_STATE = "com.m174k4.atmap.BANKS.ccb";

	private static SharedPreferences mPreferences;

	public BanksPreferences() {
	}

	private static synchronized SharedPreferences getSharedPreferences(Context context) {
		if (mPreferences == null) {
			mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		}
		return mPreferences;
	}

	private void setAllState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(ALL_STATE, state);
		editor.apply();
	}

	private void setDskState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(DSK_STATE, state);
		editor.apply();
	}

	private void setUbbState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(UBB_STATE, state);
		editor.apply();
	}

	private void setFibState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(FIB_STATE, state);
		editor.apply();
	}

	private void setRaifaizenState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(RAIFAIZEN_STATE, state);
		editor.apply();
	}

	private void setUnicreditState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(UNICREDIT_STATE, state);
		editor.apply();
	}

	private void setSgebState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(SGEB_STATE, state);
		editor.apply();
	}

	private void setAlianzState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(ALIANZ_STATE, state);
		editor.apply();
	}

	private void setPiraeusState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(PIRAEUS_STATE, state);
		editor.apply();
	}

	private void setIbankState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(IBANK_STATE, state);
		editor.apply();
	}

	private void setIabState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(IAB_STATE, state);
		editor.apply();
	}

	private void setMunicipalState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(MUNICIPAL_STATE, state);
		editor.apply();
	}

	private void setPostbankState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(POSTBANK_STATE, state);
		editor.apply();
	}

	private void setProcreditState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(PROCREDIT_STATE, state);
		editor.apply();
	}

	private void setCibankState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(CIBANK_STATE, state);
		editor.apply();
	}

	private void setTokudaState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(TOKUDA_STATE, state);
		editor.apply();
	}

	private void setDbankState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(DBANK_STATE, state);
		editor.apply();
	}

	private void setCcbState(Context context, boolean state) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(CCB_STATE, state);
		editor.apply();
	}

	public boolean getAllState(Context context) {
		return getSharedPreferences(context).getBoolean(ALL_STATE, true);
	}

	private boolean getDskState(Context context) {
		return getSharedPreferences(context).getBoolean(DSK_STATE, false);
	}

	private boolean getUbbState(Context context) {
		return getSharedPreferences(context).getBoolean(UBB_STATE, false);
	}

	private boolean getFibState(Context context) {
		return getSharedPreferences(context).getBoolean(FIB_STATE, false);
	}

	private boolean getRaifaizenState(Context context) {
		return getSharedPreferences(context).getBoolean(RAIFAIZEN_STATE, false);
	}

	private boolean getUnicreditState(Context context) {
		return getSharedPreferences(context).getBoolean(UNICREDIT_STATE, false);
	}

	private boolean getSgebState(Context context) {
		return getSharedPreferences(context).getBoolean(SGEB_STATE, false);
	}

	private boolean getAlianzState(Context context) {
		return getSharedPreferences(context).getBoolean(ALIANZ_STATE, false);
	}

	private boolean getPiraeusState(Context context) {
		return getSharedPreferences(context).getBoolean(PIRAEUS_STATE, false);
	}

	private boolean getIbankState(Context context) {
		return getSharedPreferences(context).getBoolean(IBANK_STATE, false);
	}

	private boolean getIabState(Context context) {
		return getSharedPreferences(context).getBoolean(IAB_STATE, false);
	}

	private boolean getMunicipalState(Context context) {
		return getSharedPreferences(context).getBoolean(MUNICIPAL_STATE, false);
	}

	private boolean getPostbankState(Context context) {
		return getSharedPreferences(context).getBoolean(POSTBANK_STATE, false);
	}

	private boolean getProcreditState(Context context) {
		return getSharedPreferences(context).getBoolean(PROCREDIT_STATE, false);
	}

	private boolean getCibankState(Context context) {
		return getSharedPreferences(context).getBoolean(CIBANK_STATE, false);
	}

	private boolean getTokudaState(Context context) {
		return getSharedPreferences(context).getBoolean(TOKUDA_STATE, false);
	}

	private boolean getDbankState(Context context) {
		return getSharedPreferences(context).getBoolean(DBANK_STATE, false);
	}

	private boolean getCcbState(Context context) {
		return getSharedPreferences(context).getBoolean(CCB_STATE, false);
	}

	public void clear(Context context) {
		final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.clear();
		editor.apply();
	}

	public Banks getBank(int id) {
		switch (id) {
			case R.id.menu_DSK:
				return Banks.DSK;
			case R.id.menu_UBB:
				return Banks.UBB;
			case R.id.menu_FIB:
				return Banks.FIB;
			case R.id.menu_Raifaizen:
				return Banks.RAIFAIZEN;
			case R.id.menu_Unicredit:
				return Banks.UNICREDIT;
			case R.id.menu_SGEB:
				return Banks.SGEB;
			case R.id.menu_Allianz:
				return Banks.ALLIANZ;
			case R.id.menu_Piraeus:
				return Banks.PIRAEUS;
			case R.id.menu_IBank:
				return Banks.IBANK;
			case R.id.menu_IAB:
				return Banks.IAB;
			case R.id.menu_MunicipalBank:
				return Banks.MUNICIPAL_BANK;
			case R.id.menu_PostBank:
				return Banks.POST_BANK;
			case R.id.menu_Procredit:
				return Banks.PROCREDIT;
			case R.id.menu_CiBank:
				return Banks.CIBANK;
			case R.id.menu_Tokuda:
				return Banks.TOKUDA;
			case R.id.menu_DBank:
				return Banks.DBANK;
			case R.id.menu_CCB:
				return Banks.CCB;
			default:
				return null;
		}
	}

	public void setState(Context context, int id, boolean isChecked) {
		switch (id) {
			case R.id.menu_All:
				setAllState(context, isChecked);
				break;
			case R.id.menu_DSK:
				setDskState(context, isChecked);
				break;
			case R.id.menu_UBB:
				setUbbState(context, isChecked);
				break;
			case R.id.menu_FIB:
				setFibState(context, isChecked);
				break;
			case R.id.menu_Raifaizen:
				setRaifaizenState(context, isChecked);
				break;
			case R.id.menu_Unicredit:
				setUnicreditState(context, isChecked);
				break;
			case R.id.menu_SGEB:
				setSgebState(context, isChecked);
				break;
			case R.id.menu_Allianz:
				setAlianzState(context, isChecked);
				break;
			case R.id.menu_Piraeus:
				setPiraeusState(context, isChecked);
				break;
			case R.id.menu_IBank:
				setIbankState(context, isChecked);
				break;
			case R.id.menu_IAB:
				setIabState(context, isChecked);
				break;
			case R.id.menu_MunicipalBank:
				setMunicipalState(context, isChecked);
				break;
			case R.id.menu_PostBank:
				setPostbankState(context, isChecked);
				break;
			case R.id.menu_Procredit:
				setProcreditState(context, isChecked);
			case R.id.menu_CiBank:
				setCibankState(context, isChecked);
				break;
			case R.id.menu_Tokuda:
				setTokudaState(context, isChecked);
				break;
			case R.id.menu_DBank:
				setDbankState(context, isChecked);
				break;
			case R.id.menu_CCB:
				setCcbState(context, isChecked);
				break;
		}
	}

	public boolean getState(Context context, int id) {
		switch (id) {
			case R.id.menu_All:
				return getAllState(context);
			case R.id.menu_DSK:
				return getDskState(context);
			case R.id.menu_UBB:
				return getUbbState(context);
			case R.id.menu_FIB:
				return getFibState(context);
			case R.id.menu_Raifaizen:
				return getRaifaizenState(context);
			case R.id.menu_Unicredit:
				return getUnicreditState(context);
			case R.id.menu_SGEB:
				return getSgebState(context);
			case R.id.menu_Allianz:
				return getAlianzState(context);
			case R.id.menu_Piraeus:
				return getPiraeusState(context);
			case R.id.menu_IBank:
				return getIbankState(context);
			case R.id.menu_IAB:
				return getIabState(context);
			case R.id.menu_MunicipalBank:
				return getMunicipalState(context);
			case R.id.menu_PostBank:
				return getPostbankState(context);
			case R.id.menu_Procredit:
				return getProcreditState(context);
			case R.id.menu_CiBank:
				return getCibankState(context);
			case R.id.menu_Tokuda:
				return getTokudaState(context);
			case R.id.menu_DBank:
				return getDbankState(context);
			case R.id.menu_CCB:
				return getCcbState(context);
			default:
				return false;
		}
	}

	byte[] getIv1() {
		return iv1;
	}

	private final byte[] salt2 = new byte[]{(byte) 0xfe, (byte) 0xca, (byte) 0xfc, (byte) 0xff, (byte) 0x35, (byte) 0x0b, (byte) 0xda, (byte) 0x67};

	private final byte[] iv1 = new byte[]{(byte) 0x1f, (byte) 0x36, (byte) 0xa8, (byte) 0x25, (byte) 0xbd, (byte) 0x5a, (byte) 0xc0, (byte) 0x3c};

	byte[] getSalt2() {
		return salt2;
	}

	String getP2() {
		return "VvX0BxM4";
	}
}

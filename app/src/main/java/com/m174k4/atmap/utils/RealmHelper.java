package com.m174k4.atmap.utils;

import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;

import com.m174k4.atmap.model.ATMBase;

import java.io.InputStream;
import java.nio.charset.Charset;

import io.realm.Realm;
import io.realm.internal.IOException;

public class RealmHelper {
	public void populateRealm(Realm realm, final AppCompatActivity context) {
		if (realm.isEmpty())
			populate(realm, context);
	}

	private void populate(Realm realm, final AppCompatActivity context) {
		realm.executeTransaction(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				try {
					realm.deleteAll();
					AssetManager assetManager = context.getAssets();
					InputStream input = assetManager.open("objects");
					CryptoHelper helper = new CryptoHelper();
					byte[] bytes = helper.readBytes(input);
					bytes = helper.decrypt(bytes);
					String json = new String(bytes, Charset.forName("UTF-8"));
					realm.createAllFromJson(ATMBase.class, json);
				} catch (IOException e) {
					throw new RuntimeException(e);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

package com.m174k4.atmap;

import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import com.m174k4.atmap.databinding.ActivityMainBinding;
import com.m174k4.atmap.utils.BanksPreferences;
import com.m174k4.atmap.utils.NavigationViewListener;
import com.m174k4.atmap.utils.RealmHelper;

import io.realm.Realm;

public abstract class NavDrawerActivity extends AppCompatActivity {

	ActivityMainBinding binding;

	Realm mRealm;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mRealm.close();
	}

	@Override
	public void onBackPressed() {
		if (binding.drawerLayout != null) {
			if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
				binding.drawerLayout.closeDrawer(GravityCompat.START);
			} else {
				super.onBackPressed();
			}
		} else {
			super.onBackPressed();
		}
	}

	void setStuff() {
		setUpRealm();
		setUpNavDrawer();
	}

	private void setUpRealm() {
//		CryptoHelper helper = new CryptoHelper();
//		RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
//				.encryptionKey(helper.getRealmKey())
//				.build();
//		mRealm = Realm.getInstance(realmConfiguration);

		mRealm = Realm.getDefaultInstance();
		RealmHelper realmHelper = new RealmHelper();
		realmHelper.populateRealm(mRealm, this);
	}

	private void setUpNavDrawer() {
		setSupportActionBar(binding.toolbar);

		final BanksPreferences prefs = new BanksPreferences();

		final int menuItemsCount = binding.navigationView.getMenu().size();
		final boolean allState = prefs.getAllState(NavDrawerActivity.this);
		for (int i = 0; i < menuItemsCount -1; i++) {
			final MenuItem menuItem = binding.navigationView.getMenu().getItem(i);
			CompoundButton switchView = (CompoundButton) menuItem.getActionView();
			if (allState && menuItem.getItemId() != R.id.menu_All) {
				setRowState(menuItem, switchView, false);
			}
			switchView.setChecked(prefs.getState(this, menuItem.getItemId()));
			switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (menuItem.getItemId() == R.id.menu_All) {
						for (int j = 1; j < menuItemsCount; j++) {
							MenuItem menuItem = binding.navigationView.getMenu().getItem(j);
							CompoundButton switchView = (CompoundButton) menuItem.getActionView();
							setRowState(menuItem, switchView, !isChecked);
						}
					}
					prefs.setState(NavDrawerActivity.this, menuItem.getItemId(), isChecked);
					handleBanksChange();
				}
			});
		}

		binding.navigationView.setNavigationItemSelectedListener(new NavigationViewListener(this));

		ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.app_name, R.string.app_name) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}
		};

		binding.drawerLayout.addDrawerListener(drawerToggle);
		drawerToggle.syncState();
	}

	private void setRowState(MenuItem menuItem, CompoundButton compoundButton, boolean state) {
		menuItem.setEnabled(state);
		compoundButton.setEnabled(state);
	}

	abstract void handleBanksChange();
}
